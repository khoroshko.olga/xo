﻿namespace XO
{
    internal class Program
    {
        const int a = 3;
        static string[,] gamePoleMap = new string[a, a];
        //Отрисовка игррового поля
        static void Display()
        {
            Console.Clear();
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    Console.Write("|" + gamePoleMap[i, j] + "|");
                }
                Console.WriteLine();
                Console.WriteLine("---------");
            }
            gamePoleMap[0, 2] = "X";
            gamePoleMap[1, 1] = "X";
            gamePoleMap[2, 0] = "X";
        }

        static void Default()
        {
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    gamePoleMap[i, j] = "&";
                }
            }
        }

        //Ход компа
        static void PCStep(string pcFigure)
        {
            Random rand = new Random();
            int indexI = rand.Next(0, a);
            int indexJ = rand.Next(0, a);
            bool res = Check(indexI, indexJ);
            if (!res)
            {
                PCStep(pcFigure);
            }
            else
                gamePoleMap[indexI, indexJ] = pcFigure;
        }

        static bool Check(int indexI, int indexJ)
        {
            if (gamePoleMap[indexI, indexJ] != "X" && gamePoleMap[indexI, indexJ] != "0")
            {
                return true;
            }
            return false;
        }

        static bool GetWinner(string figure)
        {
            int hor;
            int vert;
            int diag = 0;
            int diag2 = 0;
            for (int i = 0; i < a; i++)
            {
                hor = 0;
                vert = 0;
                for (int j = 0; j < a; j++)
                {
                    if (gamePoleMap[i, j] == figure)
                    {
                        hor++;
                        if (hor == a)
                        {
                            return true;
                        }
                    }

                    if (gamePoleMap[j, i] == figure)
                    {
                        vert++;
                        if (vert == a)
                        {
                            return true;
                        }
                    }
                }

                if (gamePoleMap[i, i] == figure)
                {
                    diag++;
                    if (diag == a)
                    {
                        return true;
                    }
                }

                if (gamePoleMap[i, a - 1 - i] == figure)
                {
                    diag2++;
                    if (diag2 == a)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        static void Main(string[] args)
        {
            Default();
            Display();
            Console.WriteLine(GetWinner("X"));
        }
    }
}